@smoke
Feature: ProductFeature
  This feature deals with Searching the Products
  Background: Navigate to the Flipkart website
    Given I navigate to the flipkart home page
    Then I should see the homepage
#
  Scenario Outline: Apply the filters
    Given I am on the homepage of Flipkart
    When I search for the product "<productname>"
    Then I should see the product page with the searched product "<productname>"
    Given I see the Product list
    When I select the "<fAssured>" and "<storage>" filter
    Then I can verify that both the filters are selected
    When I sort by High To low
    Then I can see the product details sorted
    And I can find the second Highest Price of the product
    Examples:
      | productname         |  fAssured    |  storage         |
      | Samsung Galaxy S24  |  true        |  256 GB & Above  |
#     | iphone 15 pro max   | false       | 128 - 255.9 GB |


