package com.auto.pages;

import com.auto.utility.Base;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.*;

public class ProductPage {
    private Base base;
    WebDriver driver;

    public static Logger log = LogManager.getLogger();
    HashMap<Integer, String> mapFinalProduct = new HashMap<Integer, String>();


    public ProductPage(WebDriver driver, Base base) {
        this.base = base;
        this.driver = base.getDriver();
        PageFactory.initElements(base.getDriver(), this);
    }

    @FindBy(how = How.XPATH, using = "//picture[@title='Flipkart']//img")
    public WebElement lblHeader;

    @FindBy(how = How.NAME, using = "q")
    public WebElement inputSearchBox;

    @FindBy(how = How.CLASS_NAME, using = "_4rR01T")
    public List<WebElement> listProductsName;

    @FindBy(how = How.CSS, using = "._30jeq3._1_WHN1")
    public List<WebElement> listProductsPrice;

    @FindBy(how = How.XPATH, using = "//div[@class='_3U-Vxu']//img")
    public WebElement chkfAssured;

    @FindBy(how = How.XPATH, using = "//div[@class = \"_2gmUFU _3V8rao\" and contains(text(), \"Internal Storage\")]")
    public WebElement drpInternalStorage;

    @FindBy(how = How.XPATH, using = "//div[contains(text(), \"256 GB & Above\")]")
    public WebElement chk256Above;
    @FindBy(how = How.XPATH, using = "//span[contains(text(),\"Price\")]")
    public WebElement spanPrice;

    @FindBy(how = How.XPATH, using = "//div[@class='_2gmUFU _3V8rao' and contains(text(),'RAM')]")
    public WebElement spanRAM;

    @FindBy(how = How.XPATH, using = "(//div[@class='_3ztiZO'])//div[contains(text(),'256 GB & Above')]")
    public WebElement divFilter256Applied;

    @FindBy(how = How.XPATH, using = "(//div[@class='_3ztiZO'])//div[contains(text(),'Plus (FAssured)')]")
    public WebElement divFilterFAssuredApplied;

    @FindBy(how = How.XPATH, using = "//div[@class=\"_5THWM1\"]//div[@class='_10UF8M _3LsR0e']")
    public WebElement sortHighToLow;


    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Home')]")
    public WebElement breadCrumbHome;

    private String productName;
    private String productDesc;
    private String productPrice;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductTitleText() {
        return lblHeader.getAttribute("title");
    }

    public void searchProduct(String productName) {
        inputSearchBox.sendKeys(productName, Keys.ENTER);
    }

    public void selectFAssured() {

        log.info(this.driver.getCurrentUrl());

        JavascriptExecutor js = (JavascriptExecutor) this.driver;
        js.executeScript("arguments[0].scrollIntoView(true);", spanPrice);
        if (chkfAssured.isDisplayed()) {
            chkfAssured.click();
        }
        log.info("Applied filter fAssured :" + divFilterFAssuredApplied.isDisplayed());


    }

    public boolean isFAssuredFilterApplied() {
        return divFilterFAssuredApplied.isDisplayed();
    }

    public void selectInternalStorage() {

        JavascriptExecutor js = (JavascriptExecutor) this.driver;
        js.executeScript("arguments[0].scrollIntoView(true);", spanRAM);
        if (drpInternalStorage.isDisplayed()) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", drpInternalStorage);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", chk256Above);
        }
        js.executeScript("arguments[0].scrollIntoView(true);", divFilter256Applied);

        log.info("Filters selected " + divFilter256Applied.isDisplayed());
    }

    public boolean isStorageFilterApplied() {
        return divFilter256Applied.isDisplayed();
    }

    public Integer getProductstSize() {
        return listProductsName.size();
    }

    public void displayListProducts() {
        for (WebElement productElement : listProductsName) {
            log.info(productElement.getText());
        }
    }

    public void displayFilteredProductInfo() {

        for (int i = 0; i < listProductsName.size(); i++) {
            productName = listProductsName.get(i).getText();
            WebElement priceElement = listProductsPrice.get(i);
            productPrice = priceElement.getText();

            productPrice = productPrice.replaceAll("[^0-9]", "");
            mapFinalProduct.put(Integer.valueOf(productPrice), productName);
        }
        log.info("Product Name and price fetched from UI and saved in HashMap as:" + mapFinalProduct.toString() + "<br>", true);

    }

    public int getSecondHighestPrice() {
        Set<Integer> allkeys = mapFinalProduct.keySet();
        ArrayList<Integer> arrayProductPriceList = new ArrayList<Integer>(allkeys);
        Collections.sort(arrayProductPriceList);
        for (Integer price : arrayProductPriceList) {
            log.info("The Sorted Price list is : " + price);
        }
        int secondHighestPrice = arrayProductPriceList.get(arrayProductPriceList.size() - 2);
        log.info("Second Highest Product Price is: " + secondHighestPrice + " Product name is: " + mapFinalProduct.get(secondHighestPrice), true);
        return secondHighestPrice;
    }

    public boolean displayListContainsSearchedProduct(String productName) {
        log.info("Searched Product found : " + listProductsName.stream().findFirst().get().getText());
        return listProductsName.stream().anyMatch(product -> product.getText().toLowerCase().contains(productName.toLowerCase()));
    }

    public void sortByHighToLow() {
        JavascriptExecutor js = (JavascriptExecutor) this.driver;
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", sortHighToLow);
        log.info("Sorted by High To Low");
    }


}
