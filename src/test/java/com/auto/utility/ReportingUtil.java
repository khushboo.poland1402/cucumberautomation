package com.auto.utility;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ReportingUtil {

    private Base base;
    WebDriver driver;

    public ReportingUtil(Base base) {
        this.base = base;
    }
    public byte[] takeScreenShot(){
        return ((TakesScreenshot)this.base.getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
