package com.auto.utility;


import com.auto.Drivers;
import com.auto.ConfigConstants;
import com.auto.pages.ProductPage;
import com.auto.ProductPO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Base {
    private WebDriver driver;
    private String profile;
    private String configPath;
    private Properties prop = new Properties();
    public static Logger log = LogManager.getLogger();

    private ProductPage product;

    private ProductPO productPO;
    private int countAddedProducts;

    public int getCountAddedProducts() {
        return countAddedProducts;
    }

    public void setCountAddedProducts(int countAddedProducts) {
        this.countAddedProducts = countAddedProducts;
    }

    public ProductPO getProductPO() {
        return productPO;
    }

    public void setProductPO(ProductPO productPO) {
        this.productPO = productPO;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver() throws IOException {
        readFromProperties();
        setBrowserDriver(prop.getProperty("browser"));
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public ProductPage getProduct() {
        return product;
    }

    private void setBrowserDriver(String browserName) {
        switch (browserName.toLowerCase().trim()) {
            case "chrome":
                log.info("Driver setting for Chrome");
                setDriverExe(Drivers.DriverEnum.CHROME.getResource(), "."+File.separator+"dependency"+File.separator+"chromedriver.exe");
                log.info("Fetching Chromedriver : " + "."+File.separator+"dependency"+File.separator+"chromedriver.exe");

                if (ConfigConstants.getIsLocal()) {
                    log.info("Driver running locally " +ConfigConstants.getIsLocal() );
                    driver = new ChromeDriver();
                } else {
                    log.info("Driver running on Gitlab");
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--headless");
                    options.addArguments("--no-sandbox");
                    options.addArguments("--window-size=1920,1080");
                    driver = new ChromeDriver(options);
                }
                break;
            case "firefox":
                setDriverExe(Drivers.DriverEnum.FIREFOX.getResource(), prop.getProperty("firefoxdriver"));
                log.info("Driver setting for Firefox");
                driver = new FirefoxDriver();
                break;
            case "safari":
                log.info("Driver setting for Safari");
                driver = new SafariDriver();
                break;
            case "ie":
                setDriverExe(Drivers.DriverEnum.IE.getResource(), prop.getProperty("iedriver"));
                log.info("Driver setting for IE");
                driver = new InternetExplorerDriver();
                break;
            default:
                log.info("No driver found!!");
        }
    }

    public void readFromProperties() throws IOException {

        getProfile();
        prop.load(getInputStream());
        log.info("Loading the browser as ---->" + prop.getProperty("browser"));
        String browserName = prop.getProperty("browser");
        setProp(prop);

    }

    private void getProfile() {
        profile = System.getenv("ENVIRONMENT").toLowerCase().trim();
        log.info("Environment is set to --> " + profile);
        if (profile == null || profile.isEmpty()) {
            ConfigConstants.setIsLocal(true);
            log.info("Running locally!!!");
        } else {
            ConfigConstants.setIsLocal(false);
        }
    }

    private FileInputStream getInputStream() throws FileNotFoundException {
        FileInputStream inputStream = new FileInputStream(System.getProperty("user.dir") + File.separator
                + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + profile + File.separator + "config.properties");
        return inputStream;
    }

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }

    public void setDriverExe(String key, String exePath) {
        System.setProperty(key, exePath);
    }

    public void initializePageObjects(WebDriver driver, Base base) {
        product = new ProductPage(driver, base);
    }
}
