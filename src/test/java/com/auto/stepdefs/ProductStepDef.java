package com.auto.stepdefs;

import com.auto.pages.ProductPage;
import com.auto.ProductPO;
import com.auto.utility.Base;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ProductStepDef {
    private Base base;
    WebDriver driver;
    ProductPage productPage;

    ProductPO productPO;
    public ProductStepDef(Base base) {
        this.base = base;
        productPO = new ProductPO();
        productPage= new ProductPage(base.getDriver(),base);
    }

    @Given("I navigate to the flipkart home page$")
    public void iNavigateToTheLoginPage() {
        log.info("Given i navigate");
        base.getDriver().navigate().to(base.getProp().getProperty("url"));
    }

    @Then("^I should see the homepage$")
    public void iShouldSeeTheHomepage() {
        String title = productPage.getProductTitleText();
        log.info(base.getDriver().getCurrentUrl());
        log.info("Given I see homepage with title.." + title);
        Assert.assertEquals(title,"Flipkart");
    }
    public static Logger log = LogManager.getLogger();
    @Given("I am on the homepage of Flipkart")
    public void iNavigateToTheProductPageForTheFirstTime() {

    }

    @When("I search for the product {string}")
    public void iSearchTheProduct(String productName) {
        productPage.searchProduct(productName);
        log.info("Searched the product.." +  productName);
    }


    @Then("I should see the product page with the searched product {string}")
    public void iShouldSeeThatTheSearchedProducts(String productName) {
        productPage.displayListProducts();
        Assert.assertTrue(productPage.displayListContainsSearchedProduct(productName));
        log.info("I should see that the basket is empty." + productPage.displayListContainsSearchedProduct(productName));
    }
//
    @Given("I see the Product list")
    public void iSeeTheProductList() {
        productPage.displayListProducts();
    }

    @When("I select the {string} and {string} filter")
    public void iSelectBothTheFilters(String fAssrued, String storage) {
        productPage.selectFAssured();
        productPage.selectInternalStorage();
        log.info("The filter selection has happened successfully");
    }

    @Then("I can verify that both the filters are selected")
    public void iCanVerifyThatBothTheFiltersAreSelected() {
        Assert.assertTrue(productPage.isFAssuredFilterApplied()) ;
        Assert.assertTrue(productPage.isStorageFilterApplied());
    }

    @When("I sort by High To low")
    public void iCanSortHighToLow() {
        productPage.sortByHighToLow();
    }

    @Then("I can see the product details sorted")
    public void iCanSeeTheResultsSorted() {
        productPage.displayFilteredProductInfo();
        log.info("Scanned all the details in the product page");
    }
    @Then("I can find the second Highest Price of the product")
    public void iCanFindTheSecondHighestPrice() {
        productPage.getSecondHighestPrice();
        log.info("Scanned all the details in the product page");
    }

}
